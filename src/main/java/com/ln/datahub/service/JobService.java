package com.ln.datahub.service;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;
import java.util.List;

import com.ln.datahub.domain.Job;

@Service
public class JobService {

  /* injecting dao...
  In dao class:
  @Repository
  public class MiscDao {
    ...
  }

  @Autowired
  private MiscDao miscDao;
  */

  public List<Job> getAllJobs() {
    Job j = new Job();
    j.setName("Test job 1");
    ArrayList<Job> jlist = new ArrayList<>();
    jlist.add(j);
    return jlist;
  }

}

