package com.ln.datahub.domain;

public class Job implements java.io.Serializable {

  private String name;
   
  public Job() {
  }

  public String getName() {
    return this.name;
  }

  public void setName(String val) {
    this.name = val;
  }

}
