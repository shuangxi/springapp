package com.ln.datahub.controller;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.GetMapping;

import com.ln.datahub.service.JobService;

@Controller
@RequestMapping("/jobs")
public class JobController {
  private static final String MY_JOBS_VIEW="listJobs";
  private static final String MY_JOBS_MODEL_ATTRIBUTE="jobList";

  @Resource
  private JobService jobService;

  /*
  @Autowired
  private JobService jobService;
  */

  @GetMapping("/list")
  public String getMyRequests(Model model){
    model.addAttribute(MY_JOBS_MODEL_ATTRIBUTE, jobService.getAllJobs());
    return MY_JOBS_VIEW;
  }
}
